// Fill out your copyright notice in the Description page of Project Settings.

#include "Commuter.h"

#include "Engine/CollisionProfile.h"
#include "SingleLaneMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/WidgetComponent.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
ACommuter::ACommuter()
{
	PrimaryActorTick.bCanEverTick = true;
	bPlayIntroductionCue = true;
	FirstRenderTolerance = 1.f;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Vehicle"));
	SetRootComponent(Mesh);

	// Set up default values for the mesh
	{
		Mesh->AlwaysLoadOnServer = true;
		Mesh->AlwaysLoadOnClient = true;
		Mesh->bOwnerNoSee = false;
		Mesh->bCastDynamicShadow = true;
		Mesh->BodyInstance.bNotifyRigidBodyCollision = true;
		Mesh->bGenerateOverlapEvents = true;
		Mesh->SetSimulatePhysics(false);

		Mesh->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	}

	DriverWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Driver ID"));
	DriverWidget->SetupAttachment(Mesh);

	// Set default values for the driver widget
	{
		DriverWidget->SetWidgetSpace(EWidgetSpace::Screen);
		DriverWidget->SetDrawSize(FVector2D(150.f, 150.f));
		DriverWidget->SetRelativeLocation(FVector(-325.f, 0.f, 200.f));
		DriverWidget->SetRelativeRotation(FRotator(90.f, 0.f, 0.f));
		DriverWidget->SetSimulatePhysics(false);
		DriverWidget->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	LaneMovement = CreateDefaultSubobject<USingleLaneMovementComponent>("Movement Component");
	AddOwnedComponent(LaneMovement);

	// Set default values for the movement component
	{
		LaneMovement->UpdatedComponent = Mesh;
		LaneMovement->Speed = 200.f;
		LaneMovement->bAutoActivate = false;
	}
}

// Called when the game starts or when spawned
void ACommuter::BeginPlay()
{
	Super::BeginPlay();

	// Make sure we are active to check if we have been rendered
	SetActorTickEnabled(true);
}

void ACommuter::PostInitProperties()
{
	Super::PostInitProperties();

	LaneIndex = -1;
}

// Called every frame
void ACommuter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// We are only checking if we are rendered. We shouldn't have
	// to check if we have already done so since we are disabling
	// tick after the introduction cue has played

	UWorld* World = Mesh->GetWorld();
	if (World && (World->GetTimeSeconds() - Mesh->LastRenderTimeOnScreen) <= FirstRenderTolerance)
	{	
		if (bPlayIntroductionCue)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), IntroductionCue);
		}

		SetActorTickEnabled(false);
	}
}

void ACommuter::ResetMeshRenderTime()
{
	Mesh->LastRenderTimeOnScreen = -1000.f;
}

