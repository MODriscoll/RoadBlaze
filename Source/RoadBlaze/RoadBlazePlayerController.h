// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RoadBlazePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ROADBLAZE_API ARoadBlazePlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	// Sets default values
	ARoadBlazePlayerController();
	
protected:

	// Called when the game starts
	virtual void BeginPlay() override;
};
