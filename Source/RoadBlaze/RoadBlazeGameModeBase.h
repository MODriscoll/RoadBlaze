// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Road.h"
#include "GameFramework/GameModeBase.h"
#include "RoadBlazeGameModeBase.generated.h"

class ACommuter;
class ARoadBlazeLevelScriptActor;

class UGameOverWidget;

// TODO: Move to data asset
USTRUCT(BlueprintType)
struct ROADBLAZE_API FRoadBlazeDriver
{

	GENERATED_BODY()

public:

	FRoadBlazeDriver()
	{

	}

public:

	// The image of the driver
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* DriverImage;

	// A quote from the driver
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USoundBase* DriverQuote;
};

USTRUCT(BlueprintType)
struct ROADBLAZE_API FCommuterSpawnRanges
{
	
	GENERATED_BODY()

public:

	FCommuterSpawnRanges()
	{
		InitialMinimumDelay = 2.f;
		FinalMinimumDelay = 1.f;

		InitialMaximumDelay = 3.f;
		FinalMaximumDelay = 2.f;
		
		TimeToReachFinal = 180.f;

		InitialTime = 0.f;
	}

	// Fixes the ranges so max is greater than min
	void FixRanges();

	// Returns the alpha value between the initial
	// spawn start time and the final time
	float GetAlpha(float CurrentTime);

	// Returns the lerped values between the initial and
	// final spawn ranges using the given alpha
	void GetRange(float Alpha, float& out_Min, float& out_Max);

public:

	// The initial minimum amount of time before spawning a commuter
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta = (ClampMin = "0", UIMin = "0"))
	float InitialMinimumDelay;

	// The final minimum amount of time before spawning a commuter
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta = (ClampMin = "0", UIMin = "0"))
	float FinalMinimumDelay;

	// The initial maximum amount of time before spawning a commuter
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta = (ClampMin = "0", UIMin = "0"))
	float InitialMaximumDelay;

	// The final maximum amount of time before spawning a commuter
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta = (ClampMin = "0", UIMin = "0"))
	float FinalMaximumDelay;

	// The time it will take to reach the final range in seconds
	UPROPERTY(EditAnywhere, BlueprintReadOnly,
		meta = (ClampMin = "1"))
	float TimeToReachFinal;

	// The initial time the spawning started
	UPROPERTY(BlueprintReadWrite)
	float InitialTime;
};

/**
 * Handles the timing and speed for the commuters in the level. As well as displaying 
 * game elements and handling level resetting
 */
UCLASS()
class ROADBLAZE_API ARoadBlazeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	// Sets default values
	ARoadBlazeGameModeBase();

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// Called when constructed
	virtual void OnConstruction(const FTransform& Transform) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:

	// Adds the game over widget to the screen. This needs to be
	// called externally, as the game mode won't call it automatically
	UFUNCTION(BlueprintCallable, Category = "Road Blaze")
	void AddGameOverWidgetToScreen();

protected:

	// Called when the player has died, ends the game
	UFUNCTION(Category = "Road Blaze", BlueprintImplementableEvent)
	void OnGameOver();

private:

	// Sets the level reference
	void SetLevelReference();

	// Initializes values for the current level
	void InitLevelReference();

	// Updates the spawn ratio and set the spawn interval timer
	void SetNextSpawn();

	// Activates a commuter
	void ActivateNewCommuter();

	// Called when a commuter has just been spawned. 
	// Sets the speed of the commuter
	UFUNCTION()
	void CommuterSpawned(ACommuter* Commuter);

	// Called when a commuter has just been despawned.
	UFUNCTION()
	void CommuterDespawned(ACommuter* Commuter);

	// Called when the players dodger has hit something
	UFUNCTION()
	void OnDodgerHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
public:

	// The ranges for spawning in new commuters
	UPROPERTY(Category = "Spawning", EditAnywhere, BlueprintReadOnly)
	FCommuterSpawnRanges DelayRanges;

	// The initial speed to set the commuters to
	UPROPERTY(Category = "Spawning", EditAnywhere, BlueprintReadOnly,
		meta = (ClampMin = "1"))
	float InitialSpeed;
	
	// The time for the speed to reach the max speed and for the min/max
	// spawn delays to reach the final spawn range delay times
	UPROPERTY(Category = "Spawning", EditAnywhere, BlueprintReadWrite,
		meta = (ClampMin = "0.0001"))
	float SpeedModifier;

	// The max speed a commuter can be set to
	UPROPERTY(Category = "Spawning", EditAnywhere, BlueprintReadWrite,
		meta = (ClampMin = "1"))
	float MaxSpeed;

	// The game over widget to add to screen on game over
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UGameOverWidget> GameOverWidgetTemplate;

private:

	// The maximum amount of commuters that will be active at once
	UPROPERTY(Category = "Game", EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	int32 MaxCommuters;

	// Reference to the level handling the commuters
	UPROPERTY(Transient)
	ARoadBlazeLevelScriptActor* LevelReference;

	// List of all the commuters currently active
	TDoubleLinkedList<ACommuter*> ActiveCommuters;

	// Timer handle for responsible for spawning new commuters
	FTimerHandle SpawnTimerHandle;

	// The current speed of commuters. Commuters
	// will be set to this value when spawned
	UPROPERTY(Category = "Spawning", BlueprintReadOnly, Transient, meta = (AllowPrivateAccess = "true"))
	float CurrentSpeed;

	// Reference to the game over widget
	UPROPERTY()
	UGameOverWidget* GameOverWidget;

	// The final score of the player
	// TODO: Move to players dodger maybe?
	float FinalScore;

	// The drivers in this road blaze game
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<FRoadBlazeDriver> Drivers;

public:

	/** Get the time since the game started (Vehicles started to spawn) */
	UFUNCTION(BlueprintPure, Category = "Road Blaze")
	float GetGameTimeSinceStart() const;
};
