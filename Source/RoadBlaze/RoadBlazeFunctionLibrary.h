// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "RoadBlazeFunctionLibrary.generated.h"

/**
 * Helper functions used throughout road blaze
 */
UCLASS()
class ROADBLAZE_API URoadBlazeFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	
	/** If game is currently running on an android device */
	UFUNCTION(BlueprintPure, Category = "Platform")
	static bool IsAndroidPlatform();
};
