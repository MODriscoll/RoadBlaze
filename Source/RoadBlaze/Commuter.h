// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Commuter.generated.h"

UCLASS()
class ROADBLAZE_API ACommuter : public APawn
{
	GENERATED_BODY()

public:

	// Sets default values for this pawn's properties
	ACommuter();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called after properties have been initialized
	virtual void PostInitProperties() override;

public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Resets this commuters tick, setting the last
	// rendering time to now
	void ResetMeshRenderTime();

	FORCEINLINE class UWidgetComponent* GetDriverWidgetComponent() const { return DriverWidget; }
	FORCEINLINE class USingleLaneMovementComponent* GetLaneMovementComponent() const { return LaneMovement; }

public:

	// Index of the lane this commuter spawned in
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
	int32 LaneIndex;

	// The tolerance before we considered rendered. This will allow
	// the commuter to appear on screen before playing the introduction cue
	UPROPERTY(Category = "Commuter", EditAnywhere, BlueprintReadWrite)
	float FirstRenderTolerance;

	// If we should play our introduction cue
	UPROPERTY(Category = "Commuter", EditAnywhere, BlueprintReadWrite)
	uint32 bPlayIntroductionCue : 1;

	// The sound to play when rendered for the first time
	UPROPERTY(BlueprintReadWrite)
	USoundBase* IntroductionCue;

private:

	// Movement component that simply moves this commuter forwards
	UPROPERTY(Category = "Vehicle", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USingleLaneMovementComponent* LaneMovement;

	// Physical representation of the commuter in the world, will be manipulated by the lane component
	UPROPERTY(Category = "Vehicle", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Mesh;

	// The widge to display behind the commuter. This should
	// be a Driver ID widget so the drivers photo can be set
	UPROPERTY(Category = "Vehicle", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* DriverWidget;
};
