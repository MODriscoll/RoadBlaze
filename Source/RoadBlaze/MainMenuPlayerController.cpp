// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuPlayerController.h"

#include "UserWidget.h"

AMainMenuPlayerController::AMainMenuPlayerController()
{
	MenuWidgetTemplate = nullptr;
	bShowMouseCursor = true;
}

void AMainMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;

	if (MenuWidgetTemplate)
	{
		MenuWidget = CreateWidget<UUserWidget>(this, MenuWidgetTemplate);
		MenuWidget->AddToViewport();
	}
}
