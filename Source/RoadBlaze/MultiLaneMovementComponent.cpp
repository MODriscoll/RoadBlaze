// Fill out your copyright notice in the Description page of Project Settings.

#include "MultiLaneMovementComponent.h"

UMultiLaneMovementComponent::UMultiLaneMovementComponent()
{
	bTickBeforeOwner = false;

	Speed = 300.f;
	bClampInput = true;
}

void UMultiLaneMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	InputScalar = 0.f;
}

void UMultiLaneMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	if (ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (UpdatedComponent)
	{
		float Scalar = ConsumeInput();

		FVector Direction = UpdatedComponent->GetRightVector() * Scalar;
		FVector DesiredMovement = Direction * Speed;

		DesiredMovement *= DeltaTime;

		// Don't bother trying to move if we wouldn't be moving
		if (!DesiredMovement.IsNearlyZero())
		{
			FHitResult Hit;
			SafeMoveUpdatedComponent(DesiredMovement, UpdatedComponent->GetComponentRotation(), true, Hit);

			// Try sliding along any hit surface
			if (Hit.IsValidBlockingHit())
			{
				SlideAlongSurface(DesiredMovement, 1.f - Hit.Time, Hit.Normal, Hit, true);
			}
		}
	}
}

void UMultiLaneMovementComponent::AddMovementInput(float InputValue)
{
	InputScalar += InputValue;
}

float UMultiLaneMovementComponent::ConsumeInput()
{
	float Input = InputScalar;

	if (bClampInput)
	{
		// Clamp to unit value to prevent
		// delta vectors magnitude from passing
		// the set speed
		Input = FMath::Clamp(Input, -1.f, 1.f);
	}

	InputScalar = 0.f;

	return Input;
}
