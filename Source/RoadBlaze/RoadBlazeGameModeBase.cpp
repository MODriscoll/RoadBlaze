// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadBlazeGameModeBase.h"

#include "Commuter.h"
#include "Dodger.h"

#include "Components/SkeletalMeshComponent.h"
#include "SingleLaneMovementComponent.h"
#include "Components/WidgetComponent.h"

#include "RoadBlazePlayerController.h"
#include "RoadBlazeLevelScriptActor.h"

#include "Engine/World.h"
#include "Engine/StreamableManager.h"
#include "TimerManager.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

#include "GameOverWidget.h"
#include "DriverIDWidget.h"

ARoadBlazeGameModeBase::ARoadBlazeGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;

	MaxCommuters = 20;

	DelayRanges.InitialMinimumDelay = 2.f;
	DelayRanges.InitialMaximumDelay = 3.f;

	DelayRanges.FinalMinimumDelay = 1.f;
	DelayRanges.FinalMaximumDelay = 2.f;

	DelayRanges.TimeToReachFinal = 180.f;

	InitialSpeed = 300.f;
	MaxSpeed = 900.f;
	SpeedModifier = 2.f;

	DefaultPawnClass = ADodger::StaticClass();
	PlayerControllerClass = ARoadBlazePlayerController::StaticClass();
	bPauseable = false;
}

void ARoadBlazeGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// First get reference to the level, then try
	// initializing it before attempting to start the game
	SetLevelReference();
	InitLevelReference();

	CurrentSpeed = InitialSpeed;
	DelayRanges.InitialTime = UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld());
}

void ARoadBlazeGameModeBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	DelayRanges.FixRanges();
}

void ARoadBlazeGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurrentSpeed += SpeedModifier * DeltaTime;
	CurrentSpeed = FMath::Min(CurrentSpeed, MaxSpeed);
}

void ARoadBlazeGameModeBase::AddGameOverWidgetToScreen()
{
	if (GameOverWidgetTemplate)
	{
		GameOverWidget = CreateWidget<UGameOverWidget>(GetWorld(), GameOverWidgetTemplate);
		GameOverWidget->AddToViewport();

		GameOverWidget->SetScore(FinalScore);
	}
}

void ARoadBlazeGameModeBase::SetLevelReference()
{
	UWorld* World = GetWorld();
	if (World)
	{
		ALevelScriptActor* Level = World->GetLevelScriptActor();
		LevelReference = Cast<ARoadBlazeLevelScriptActor>(Level);

		if (!LevelReference)
		{
			UE_LOG(LogGameMode, Warning, TEXT("Level script actor is not of road blaze type. Will be unable to implement game mode"));
		}
	}
	else
	{
		LevelReference = nullptr;
	}
}

void ARoadBlazeGameModeBase::InitLevelReference()
{
	if (LevelReference)
	{
		LevelReference->InitCommuterPool(MaxCommuters);
		LevelReference->InitRoad();
		LevelReference->InitPlayer();

		LevelReference->OnCommuterSpawn.AddDynamic(this, &ARoadBlazeGameModeBase::CommuterSpawned);
		LevelReference->OnCommuterDespawn.AddDynamic(this, &ARoadBlazeGameModeBase::CommuterDespawned);

		if (LevelReference->PlayersDodger)
		{
			// Bind out check if game over function to the players
			// hit event.
			USkeletalMeshComponent* DodgerMesh = LevelReference->PlayersDodger->GetMesh();
			DodgerMesh->OnComponentHit.AddDynamic(this, &ARoadBlazeGameModeBase::OnDodgerHit);
		}
		else
		{
			UE_LOG(LogGameMode, Error, TEXT("Level has not player. Will be unable to end the game!"));
		}
		
		// Initialize the spawning system
		SetNextSpawn();
	}
}

void ARoadBlazeGameModeBase::SetNextSpawn()
{
	FTimerManager& Manager = GetWorldTimerManager();

	// Don't reset the timer if a commuter is waiting to spawn
	if (Manager.IsTimerActive(SpawnTimerHandle))
	{
		return;
	}

	float Min, Max;
	
	// Get the alpha based on the time since the game started so
	// we can get the current spawn delay range to use
	float Alpha = DelayRanges.GetAlpha(UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld()));
	DelayRanges.GetRange(Alpha, Min, Max);

	float RandomDelay = FMath::RandRange(Min, Max);

	Manager.SetTimer(SpawnTimerHandle, this,
		&ARoadBlazeGameModeBase::ActivateNewCommuter, RandomDelay, false);
}

void ARoadBlazeGameModeBase::ActivateNewCommuter()
{
	if (LevelReference)
	{
		LevelReference->SpawnCommuters(1);

		// Set the spawn for the next commuter
		SpawnTimerHandle.Invalidate();
		SetNextSpawn();
	}
}

void ARoadBlazeGameModeBase::CommuterSpawned(ACommuter* Commuter)
{
	USingleLaneMovementComponent* LaneMovement = Commuter->GetLaneMovementComponent();

	LaneMovement->Speed = CurrentSpeed;

	// Set a random driver to be in control of this commuter
	{
		float RandomIndex = FMath::RandRange(0, Drivers.Num() - 1);
		if (Drivers.IsValidIndex(RandomIndex))
		{
			const FRoadBlazeDriver& DriverID = Drivers[RandomIndex];

			Commuter->IntroductionCue = DriverID.DriverQuote;

			UWidgetComponent* Widget = Commuter->GetDriverWidgetComponent();
			UDriverIDWidget* DriverWidget = Cast<UDriverIDWidget>(Widget->GetUserWidgetObject());
			if (DriverWidget)
			{
				DriverWidget->SetDriverImage(DriverID.DriverImage);
			}
		}
	}

	// We also need to reset the commuters tick
	Commuter->SetActorTickEnabled(true);

	//ActiveCommuters.AddTail(Commuter);
}

void ARoadBlazeGameModeBase::CommuterDespawned(ACommuter* Commuter)
{
	//ActiveCommuters.RemoveNode(Commuter);

	// Reset the commuters last render time, so its
	// treated like it was never on screen
	Commuter->ResetMeshRenderTime();
}

void ARoadBlazeGameModeBase::OnDodgerHit(UPrimitiveComponent* HitComponent,
	AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// We are only interested if the player has collided with a commuter
	ACommuter* Commuter = Cast<ACommuter>(OtherActor);
	if (Commuter)
	{
		if (LevelReference && LevelReference->PlayersDodger)
		{
			USkeletalMeshComponent* DodgerMesh = LevelReference->PlayersDodger->GetMesh();
			DodgerMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

			// Disables the playes input, so they can't move anymore
			APlayerController* Controller = Cast<APlayerController>(LevelReference->PlayersDodger->GetController());
			if (Controller)
			{
				Controller->SetInputMode(FInputModeUIOnly());
				Controller->bShowMouseCursor = true;
			}

			DodgerMesh->SetVisibility(false, true);
		}

		FinalScore = GetGameTimeSinceStart();

		OnGameOver();
	}
}

float ARoadBlazeGameModeBase::GetGameTimeSinceStart() const
{
	return UKismetSystemLibrary::GetGameTimeInSeconds(GetWorld()) - DelayRanges.InitialTime;
}

float FCommuterSpawnRanges::GetAlpha(float CurrentTime)
{
	float TimePassed = CurrentTime - InitialTime;

	float Alpha = TimePassed / TimeToReachFinal;

	// Make sure to clamp the value
	return FMath::Clamp(Alpha, 0.f, 1.f);
}

void FCommuterSpawnRanges::FixRanges()
{
	InitialMaximumDelay = FMath::Max(InitialMinimumDelay, InitialMaximumDelay);
	FinalMaximumDelay = FMath::Max(FinalMinimumDelay, FinalMaximumDelay);
}

void FCommuterSpawnRanges::GetRange(float Alpha, float& out_Min, float& out_Max)
{
	out_Min = FMath::Lerp(InitialMinimumDelay, FinalMinimumDelay, Alpha);
	out_Max = FMath::Lerp(InitialMaximumDelay, FinalMaximumDelay, Alpha);
}
