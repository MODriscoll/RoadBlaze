// Fill out your copyright notice in the Description page of Project Settings.

#include "GameOverWidget.h"

#include "Components/TextBlock.h"

#include "Kismet/KismetMathLibrary.h"

#define LOCTEXT_NAMESPACE "RoadBlaze"

void UGameOverWidget::SetScore(float Score, bool bConvertToMinutes)
{
	UTextBlock* TextBlock = GetScoreText();
	if (TextBlock)
	{
		FText ScoreText = FText::AsNumber(Score);

		// First convert the score to minutes if desired
		if (bConvertToMinutes)
		{
			float Remainder;
			int32 Minutes = UKismetMathLibrary::FMod(Score, 60.f, Remainder);
			int32 Seconds = static_cast<int32>(Remainder);

			FFormatNamedArguments Args;
			Args.Add(TEXT("Minutes"), Minutes);
			Args.Add(TEXT("Seconds"), Seconds);

			FText FormatText;
			if (Seconds < 10)
			{
				FormatText = LOCTEXT("RoadBlaze", "{Minutes}:0{Seconds}");
			}
			else
			{
				FormatText = LOCTEXT("RoadBlaze", "{Minutes}:{Seconds}");
			}
			
			ScoreText = FText::Format(FormatText, Args);
		}

		// Then apply formatting if also desired
		if (!FormatString.IsEmpty())
		{
			// Try to apply the score into the formatting
			// of the set format string. It's up to the user
			// to make sure the string supports formatting
			FFormatNamedArguments Args;
			Args.Add(TEXT("Score"), ScoreText);
			ScoreText = FText::Format(FormatString, Args);
		}

		TextBlock->SetText(ScoreText);
	}
}

#undef LOCTEXT_NAMESPACE

