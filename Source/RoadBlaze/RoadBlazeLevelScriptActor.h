// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Road.h"
#include "Engine/LevelScriptActor.h"
#include "RoadBlazeLevelScriptActor.generated.h"

class ACommuter;
class ADodger;

// Delegates for when a commuter has been activated or de-activated
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCommuterActivationSignature, ACommuter*, Commuter);

// Delegate for when the player has just died
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDodgerDeathSignature, ADodger*, Player, ACommuter*, Hitby);

// Node storing a commuter and if its active
USTRUCT(BlueprintType)
struct ROADBLAZE_API FCommuterNode
{

	GENERATED_BODY()

public:

	FCommuterNode()
	{
		Commuter = nullptr;
		bIsActive = false;
	}

	UPROPERTY(Category = "Pool", VisibleAnywhere, BlueprintReadOnly, Transient)
	ACommuter* Commuter;

	UPROPERTY(Category = "Pool", VisibleAnywhere, BlueprintReadOnly, Transient)
	uint32 bIsActive : 1;
};

// Object pool specifically designed for commuters (normal pool would be designed
// to be inherited from)
// TODO: Refactor into own container type (including node and iterator)
USTRUCT(BlueprintType)
struct ROADBLAZE_API FCommuterPool
{

	GENERATED_BODY()

public:

	enum EIterateType : int32
	{
		Forward = 1,
		Reverse = -1
	};

public:

	// Sets default values
	FCommuterPool();

	// Destroys all actors in the pool
	//~FCommuterPool();

public:

	// Re-Initializes the size of the pool, destroying
	// outdated commuters or creating new ones if required
	void Resize(int32 NewSize, UWorld* World, const TFunction<void(ACommuter*)>& Callback);

	// From the giving index, iterates to the next
	// commuter matching the active parameter. Index will
	// be invalid if iteration reached the end of the pool
	int32 Iterate(int32 StartIndex, bool bIterateActive, EIterateType IterateType = Forward);

	// From the given index, iterates to the next
	// commuter. Index will be invalid if iteration
	// reached end of the pool
	int32 IterateAll(int32 StartIndex, EIterateType IterateType = Forward);

	// Activates inactive commuters in the pool and calls the predicate,
	// passing an activated commuter that was just activated
	void Activate(int32 NumberToActivate, const TFunction<void(ACommuter*)>& Callback);

	// De-activates the given commuter. Returns if
	// commuter was just set to inactive (if commuter
	// was active but not inactive).
	bool Deactivate(ACommuter* Commuter);

	// De-activates a commuter by index, returns the commuter
	// that was just de-activated. If commuter was already
	// inactive, a null value is returned
	ACommuter* Deactivate(int32 Index);

	// Returns if commuter at given index is active. It's important
	// to note this function does not check if index is valid
	inline bool IsCommuterActive(int32 Index) const
	{
		const FCommuterNode& Node = Pool[Index];
		return Node.bIsActive;
	}

	// Returns the commuter at the given node. Return
	// value will be invalid if index is also invalid
	inline ACommuter* GetCommuterAt(int32 Index) const
	{
		if (Pool.IsValidIndex(Index))
		{
			const FCommuterNode& Node = Pool[Index];
			return Node.Commuter;
		}

		return nullptr;
	}

	inline FCommuterNode& operator [] (int32 Index) { return Pool[Index]; }
	inline const FCommuterNode& operator [] (int32 Index) const { return Pool[Index]; }
	inline bool IsValidIndex(int32 Index) const { return Pool.IsValidIndex(Index); }
	inline int32 Num() const { return Pool.Num(); }

public:

	// The type of commuter to spawn
	UPROPERTY(Category = "Pool", EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ACommuter> CommuterTemplate;

private:

	UPROPERTY(Category = "Pool", VisibleInstanceOnly, Transient)
	TArray<FCommuterNode> Pool;
};

/**
 * Handles the pool of commuters in the world. Designed to work with the
 * game mode of a similar name. For the actor references, set those in the
 * level blueprints.
 */
UCLASS()
class ROADBLAZE_API ARoadBlazeLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()

public:

	// Sets default values
	ARoadBlazeLevelScriptActor();
	
protected:

#if WITH_EDITOR

		// Called when a property has been changed in the editor
		virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

#endif
	
public:

	// Initializes the commuter pool to given size
	UFUNCTION(BlueprintCallable)
	void InitCommuterPool(int32 Size);

	// Initializes the road for use. We set the given players view
	// target to use the roads viewing camera
	UFUNCTION(BlueprintCallable)
	void InitRoad();

	// Initializes the player. Does not set the players view target,
	// as that depends on the road
	void InitPlayer();

	// Spawns the specified amount of commuters
	void SpawnCommuters(int32 Amount);

	// Hides a commuter to the offscreen location
	void HideCommuter(ACommuter* Commuter);

	FORCEINLINE const FCommuterPool& GetCommuterPool() const { return Commuters; }

private:

	// Callback for when a commuter reached the end of the road
	UFUNCTION()
	void OnCommuterAtEnd(ACommuter* Commuter, const FLane& Lane, int32 LaneIndex);

	// Callback for when the dodger has hit something
	UFUNCTION()
	void OnDodgerHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	// 'Respawns' a commuter, meaning a random lane is set for the given commuter
	void RespawnCommuter(ACommuter* Commuter);

public:

	// Delegate called when a commuter has just been spawned
	UPROPERTY(BlueprintAssignable, Category = "Game")
	FCommuterActivationSignature OnCommuterSpawn;

	// Delegate called when a commuter has just been despawned
	UPROPERTY(BlueprintAssignable, Category = "Game")
	FCommuterActivationSignature OnCommuterDespawn;

	// Delegate called when the player has been hit by a commuter
	UPROPERTY(BlueprintAssignable, Category = "Game")
	FDodgerDeathSignature OnDodgerDeath;

	// If the roads lanes the be aligned on the Z axis with the player
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadWrite)
	uint32 bAlignLanesWithPlayer : 1;

	// Transform to be used to hide commuters offscreen while inactive
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadWrite)
	AActor* OffscreenCommuterPlacementActor;

	// Reference to the road this level contains
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadWrite)
	ARoad* LevelRoad;

	// Reference to the players dodging vehicle
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadOnly)
	ADodger* PlayersDodger;

private:

	// All of the commuters being handled
	UPROPERTY(Category = "Game", EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	FCommuterPool Commuters;
};
