// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Road.generated.h"

USTRUCT(BlueprintType)
struct ROADBLAZE_API FLane
{
	GENERATED_BODY()

public:

	FLane()
	{
		Transform = FTransform::Identity;
	}

	// Transforms both the origin and facing into co-ordinate space of given parant
	void GetWorldOriginAndFacing(const FTransform& Parent, FVector& out_Origin, FQuat& out_Facing) const;

public:

	// The transform for the start of this lane
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (MakeEditWidget = "true"))
	FTransform Transform;
};

// Delegate for when a commuter has reached the end of the road
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FCommuterAtEndSignature, class ACommuter*, Commuter, const FLane&, Lane, int32, LaneIndex);

UCLASS(BlueprintType)
class ROADBLAZE_API ARoad : public AActor
{
	GENERATED_BODY()
	
public:	

	// Sets default values for this actor's properties
	ARoad();

public:

	// Called when constructed
	virtual void OnConstruction(const FTransform& Transform) override;

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// Rotates all of the lanes to face towards the end of the road
	UFUNCTION(BlueprintCallable, Category = "Road")
	void FaceLanesTowardsEnd();

	// Sets the position and rotation 
	// of the given at the specified lane. Returns
	// if actor was successfully re-located
	UFUNCTION(BlueprintCallable, Category = "Road")
	bool SetActorAtLane(AActor* Actor, int32 LaneIndex);

	// Generates a random index of all the lanes. This also
	// sets the last lane index
	UFUNCTION(BlueprintPure, Category = "Road")
	int32 GetRandomLaneIndex();

	FORCEINLINE TArray<FLane>& GetLanes() { return Lanes; }

	FORCEINLINE class UCameraComponent* GetViewCamera() const { return ViewingCamera; }

private:

	// Called when the end of road box is overlapped
	UFUNCTION()
	void OnEndOfRoadReached(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:

	// Delegate that will be called when a commuter
	// overlaps with the end of the road trigger
	UPROPERTY(BlueprintAssignable, Category = "Road")
	FCommuterAtEndSignature OnCommuterReachedEnd;

	// If the lanes should face towards the end of road box. 
	// Only applies when spawning into the game
	UPROPERTY(Category = "Road", EditAnywhere)
	uint32 bFaceLanesTowardsEnd : 1;

	// If actors should be placed in the same lane more than once in a row
	UPROPERTY(Category = "Road", EditAnywhere, BlueprintReadWrite)
	uint32 bRepeatLaneOnSetLane : 1;

private:

	// Dummy root
	UPROPERTY()
	USceneComponent* Dummy;

	UPROPERTY(Category = "Components", VisibleAnywhere)
	class UBillboardComponent* Marker;

	// The collision for commuters to hit, signalling they have reached the end of the road
	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* EndOfRoad;

	// The camera at which to view the road
	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* ViewingCamera;

	// The lanes this road contains
	UPROPERTY(Category = "Road", EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TArray<FLane> Lanes;

	// The last lane that an actor was placed onto
	UPROPERTY()
	int32 LastLanePlacementIndex;
};
