// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Dodger.generated.h"


UCLASS()
class ROADBLAZE_API ADodger : public APawn
{
	GENERATED_BODY()

public:

	// Sets default values for this pawn's properties
	ADodger();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called after all components have been initialized
	virtual void PostInitializeComponents() override;

public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Moves the dodger towards the right
	UFUNCTION(BlueprintCallable)
	void MoveRight(float AxisValue);

	FORCEINLINE USkeletalMeshComponent* GetMesh() const { return Mesh; }

private:

	// Movement component that handles moving left or right across multiple lanes
	UPROPERTY(Category = "Vehicle", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMultiLaneMovementComponent* LaneMovement;
	
	// Physical representation of the dodger in the world, will be manipulated by the lane component
	UPROPERTY(Category = "Vehicle", VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* Mesh;
};
