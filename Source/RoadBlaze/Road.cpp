// Fill out your copyright notice in the Description page of Project Settings.

#include "Road.h"

#include "Components/BillboardComponent.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"

#include "Commuter.h"

ARoad::ARoad()
{
	PrimaryActorTick.bCanEverTick = false;

	Dummy = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy"));
	SetRootComponent(Dummy);

	Dummy->SetMobility(EComponentMobility::Static);

	Marker = CreateDefaultSubobject<UBillboardComponent>(TEXT("Marker"));
	Marker->SetupAttachment(Dummy);

	Marker->bHiddenInGame = true;
	Marker->bIsEditorOnly = true;

	ViewingCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Viewing Camera"));
	ViewingCamera->SetupAttachment(Dummy);

	// Set default values for this camera
	{
		ViewingCamera->SetRelativeLocation(FVector(-2500.f, 0.f, 2000.f));
		ViewingCamera->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
	}

	EndOfRoad = CreateDefaultSubobject<UBoxComponent>(TEXT("End Of Road Trigger"));
	EndOfRoad->SetupAttachment(Dummy);

	// Set default values for this box
	{
		EndOfRoad->bGenerateOverlapEvents = true;
		EndOfRoad->SetCollisionProfileName(TEXT("Trigger"));
		EndOfRoad->SetSimulatePhysics(false);
		EndOfRoad->SetBoxExtent(FVector(100.f, 2000.f, 250.f), false);
		EndOfRoad->SetRelativeLocation(FVector(-5000.f, 0.f, 0.f));
	}

	bFaceLanesTowardsEnd = true;
	bRepeatLaneOnSetLane = true;
}

void ARoad::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (bFaceLanesTowardsEnd)
	{
		FaceLanesTowardsEnd();
	}
}

void ARoad::BeginPlay()
{
	Super::BeginPlay();

	EndOfRoad->OnComponentBeginOverlap.AddDynamic(this, &ARoad::OnEndOfRoadReached);

	LastLanePlacementIndex = -1;
}

void ARoad::FaceLanesTowardsEnd()
{
	// Since end of road is child of the root, the facing vector
	// is the normalized location of the box. Also ignore vertical displacement
	FVector Displacement = EndOfRoad->GetRelativeTransform().GetLocation().GetSafeNormal2D();

	// We only want the yaw so the cars stay upright
	FRotator Direction = Displacement.ToOrientationRotator();
	Direction.Pitch = 0.f;

	FQuat AsQuat = Direction.Quaternion();

	for (FLane& Lane : Lanes)
	{
		Lane.Transform.SetRotation(AsQuat);
	}
}

bool ARoad::SetActorAtLane(AActor* Actor, int32 LaneIndex)
{
	if (Actor && Lanes.IsValidIndex(LaneIndex))
	{
		const FLane& Lane = Lanes[LaneIndex];

		FVector Location;
		FQuat Rotation;

		// Convert this lane into world space
		Lane.GetWorldOriginAndFacing(Dummy->GetComponentTransform(), Location, Rotation);

		FTransform NewTransform;
		NewTransform.SetLocation(Location);
		NewTransform.SetRotation(Rotation);
		NewTransform.SetScale3D(FVector::OneVector);
		Actor->SetActorTransform(NewTransform);

		return true;
	}

	return false;
}

int32 ARoad::GetRandomLaneIndex()
{
	int32 Index = LastLanePlacementIndex;

	// Make sure there is more than one lane
	// to prevent an infinite loop
	if (!bRepeatLaneOnSetLane && Lanes.Num() > 1)
	{
		int32 LastIndex = Lanes.Num() - 1;

		// Keep looping until a new lane is found
		while (Index == LastLanePlacementIndex)
		{
			Index = FMath::RandRange(0, LastIndex);
		}
	}
	else
	{
		Index = FMath::RandRange(0, Lanes.Num() - 1);
	}

	LastLanePlacementIndex = Index;

	return Index;
}

void ARoad::OnEndOfRoadReached(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Only respond if overlapped actor is a commuter
	ACommuter* Commuter = Cast<ACommuter>(OtherActor);
	if (Commuter)
	{
		int32 LaneIndex = Commuter->LaneIndex;
		if (Lanes.IsValidIndex(LaneIndex))
		{
			OnCommuterReachedEnd.Broadcast(Commuter, Lanes[LaneIndex], LaneIndex);
		}
	}
}

void FLane::GetWorldOriginAndFacing(const FTransform& Parent, FVector& out_Origin, FQuat& out_Facing) const
{
	out_Origin = Parent.TransformPosition(Transform.GetLocation());
	out_Facing = Parent.TransformRotation(Transform.GetRotation());
}
