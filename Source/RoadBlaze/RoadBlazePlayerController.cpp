// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadBlazePlayerController.h"

ARoadBlazePlayerController::ARoadBlazePlayerController()
{
	bShowMouseCursor = false;
}

void ARoadBlazePlayerController::BeginPlay()
{
	Super::BeginPlay();

	#if PLATFORM_ANDROID
	SetInputMode(FInputModeGameAndUI());
	#else
	SetInputMode(FInputModeGameOnly());
	#endif
	bShowMouseCursor = false;
}
