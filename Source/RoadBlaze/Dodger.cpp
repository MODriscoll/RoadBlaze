// Fill out your copyright notice in the Description page of Project Settings.

#include "Dodger.h"

#include "Engine/CollisionProfile.h"
#include "MultiLaneMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values
ADodger::ADodger()
{
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Vehicle"));
	SetRootComponent(Mesh);

	// Set up default values for the mesh
	{
		Mesh->AlwaysLoadOnServer = true;
		Mesh->AlwaysLoadOnClient = true;
		Mesh->bOwnerNoSee = false;
		Mesh->bCastDynamicShadow = true;
		Mesh->BodyInstance.bNotifyRigidBodyCollision = true;
		Mesh->bGenerateOverlapEvents = false;
		Mesh->SetSimulatePhysics(false);

		Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		Mesh->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);
	}

	LaneMovement = CreateDefaultSubobject<UMultiLaneMovementComponent>("Movement Component");
	AddOwnedComponent(LaneMovement);

	// Set default values for the movement component
	{
		LaneMovement->UpdatedComponent = Mesh;
		LaneMovement->Speed = 600.f;
	}
}

void ADodger::BeginPlay()
{
	Super::BeginPlay();
	
}

void ADodger::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	Mesh->SetNotifyRigidBodyCollision(true);
}

void ADodger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADodger::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveRight", this, &ADodger::MoveRight);
}

void ADodger::MoveRight(float AxisValue)
{
	LaneMovement->AddMovementInput(AxisValue);
}


