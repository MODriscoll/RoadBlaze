// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainMenuPlayerController.generated.h"

class UUserWidget;

/**
 * Player controller designed to work with the main menu. Will
 * set input mode to UI and spawn the menu widget
 */
UCLASS()
class ROADBLAZE_API AMainMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	
	// Sets default values
	AMainMenuPlayerController();

protected:

	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// The menu widget to spawn on game start. This widget
	// should give the player the ability to start the game
	// and close the application
	UPROPERTY(Category = "Menu", EditAnywhere, BlueprintReadOnly, meta = (DisplayName = "Menu Widget"))
	TSubclassOf<UUserWidget> MenuWidgetTemplate;

private:

	// Reference to the widget created
	UPROPERTY()
	UUserWidget* MenuWidget;
};
