// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"

class UTextBlock;

/**
 * Contains logic needed for the road blaze game mode to
 * display the users score. This menu should also provide
 * options for the player to restart and return to the main menu
 */
UCLASS()
class ROADBLAZE_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	// Visually sets the given score 
	UFUNCTION(Category = "Game Over Widget", BlueprintCallable)
	void SetScore(float Score, bool bConvertToMinutes = true);

protected:
	
	// Called internally to access the text widget
	// that will display the score to the user
	UFUNCTION(Category = "Game Over Widget", BlueprintImplementableEvent)
	UTextBlock* GetScoreText() const;

public:

	// A string that will be used to format the score into. To utialise,
	// write the string with {Score} where you would like the score to
	// appear. This can be left blank if you just want the score to be set
	UPROPERTY(Category = "Game Over Widget", EditAnywhere, BlueprintReadWrite)
	FText FormatString;
};
