// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/MovementComponent.h"
#include "SingleLaneMovementComponent.generated.h"

/**
 * Simple movement component that will move an actor an actor
 * forward like they are following a lane. The forward direction
 * used is the forward vector of the updated component
 */
UCLASS(BlueprintType)
class ROADBLAZE_API USingleLaneMovementComponent : public UMovementComponent
{
	GENERATED_BODY()
	
public:

	// Sets default values
	USingleLaneMovementComponent();
	
	// Moves the updated component forward
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

public:

	// The speed at which to progress forward. If value
	// is negative, the updated component will move backwards
	UPROPERTY(Category = "Movement", EditAnywhere, BlueprintReadWrite)
	float Speed;
};
