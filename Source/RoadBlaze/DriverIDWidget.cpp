// Fill out your copyright notice in the Description page of Project Settings.

#include "DriverIDWidget.h"

#include "Components/Image.h"

void UDriverIDWidget::SetDriverImage(UTexture2D* Image)
{
	UImage* DriverImage = GetDriverImage();
	if (DriverImage)
	{
		DriverImage->SetBrushFromTexture(Image);
	}
}
