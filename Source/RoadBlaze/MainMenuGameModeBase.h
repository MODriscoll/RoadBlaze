// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuGameModeBase.generated.h"

/**
 * Simple game mode for the main menu. Simply set default classes
 */
UCLASS()
class ROADBLAZE_API AMainMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	// Sets default values
	AMainMenuGameModeBase();
};
