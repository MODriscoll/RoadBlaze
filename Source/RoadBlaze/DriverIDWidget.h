// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DriverIDWidget.generated.h"

class UImage;

/**
 * 
 */
UCLASS()
class ROADBLAZE_API UDriverIDWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:

	// Called internally, is used to access the
	// image to set the drivers picture to
	UFUNCTION(Category = "Driver ID Widget", BlueprintImplementableEvent)
	UImage* GetDriverImage() const;
	
public:

	// Sets the image of the driver
	UFUNCTION(BlueprintCallable, Category = "Driver ID Widget")
	void SetDriverImage(UTexture2D* Image);
};
