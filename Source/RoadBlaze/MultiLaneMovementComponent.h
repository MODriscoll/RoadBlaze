// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/MovementComponent.h"
#include "MultiLaneMovementComponent.generated.h"

/**
 * Opposite of the single lane component, will only move the
 * updated component left to right like they are crossing lanes.
 * The right direction used is the right vector of the updated
 * component.
 */
UCLASS(BlueprintType)
class ROADBLAZE_API UMultiLaneMovementComponent : public UMovementComponent
{
	GENERATED_BODY()
	
public:

	// Sets default values
	UMultiLaneMovementComponent();

	// Called when play starts
	virtual void BeginPlay() override;
	
	// Moves the updated component left or right based on inputs
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	// Adds input to apply on next tick
	// Positive values will move component right, negative values left
	UFUNCTION(BlueprintCallable)
	void AddMovementInput(float InputValue);

public:

	// The speed at which to travel
	UPROPERTY(Category = "Movement", EditAnywhere, BlueprintReadWrite)
	float Speed;

	// If input should be clamped. This will prevent the
	// amount to move from being greater than the set speed
	UPROPERTY(Category = "Movement", EditAnywhere, BlueprintReadWrite)
	uint32 bClampInput : 1;

	uint32 bIsFalling : 1;

protected:

	// The input scalar, used to determine if we should move right or left
	float InputScalar;

protected:

	// Will reset movement input but returning the value before it was reset
	virtual float ConsumeInput();
};
