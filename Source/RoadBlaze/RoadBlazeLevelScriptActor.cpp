// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadBlazeLevelScriptActor.h"

#include "Commuter.h"
#include "Dodger.h"

#include "SingleLaneMovementComponent.h"
#include "Components/SkeletalMeshComponent.h"

#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"

FCommuterPool::FCommuterPool()
{
	CommuterTemplate = ACommuter::StaticClass();
}

//FCommuterPool::~FCommuterPool()
//{
//	for (const FCommuterNode& Node : Pool)
//	{
//		if (Node.Commuter)
//		{
//			Node.Commuter->Destroy();
//		}
//	}
//}

void FCommuterPool::Resize(int32 NewSize, UWorld * World, const TFunction<void(ACommuter*)>& Callback)
{
	int32 OldSize = Pool.Num();

	if (NewSize == OldSize)
	{
		return;
	}

	if (NewSize > OldSize)
	{
		if (!World)
		{
			return;
		}

		// Make sure the template is also valid
		if (!CommuterTemplate)
		{
			CommuterTemplate = ACommuter::StaticClass();
		}

		Pool.Reserve(NewSize);

		for (int32 Index = OldSize; Index < NewSize; ++Index)
		{
			FCommuterNode Node;

			// Always spawns these commuters. The actual placement
			// of these actors will be handled by the game mode
			FActorSpawnParameters Params;
			Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			Params.ObjectFlags |= EObjectFlags::RF_Transient;

			Node.Commuter = World->SpawnActor<ACommuter>(CommuterTemplate, FVector::ZeroVector, FRotator::ZeroRotator, Params);
			Node.bIsActive = false;

			// Callback should be used to reset the commuter
			Callback(Node.Commuter);

			Pool.Push(Node);
		}
	}
	else
	{
		for (int32 Index = NewSize; Index < OldSize; ++Index)
		{
			FCommuterNode& Node = Pool[Index];

			if (Node.Commuter)
			{
				Node.Commuter->Destroy();
			}
		}

		Pool.SetNum(NewSize);
	}
}

int32 FCommuterPool::Iterate(int32 StartIndex, bool bIterateActive, EIterateType IterateType)
{
	int32 Index = StartIndex;
	for (; Pool.IsValidIndex(Index); Index += IterateType)
	{
		if (IsCommuterActive(Index) == bIterateActive)
			break;
	}

	return Index;
}

int32 FCommuterPool::IterateAll(int32 StartIndex, EIterateType IterateType)
{
	int32 Index = StartIndex + IterateType;
	return Index;
}

void FCommuterPool::Activate(int32 NumberToActivate, const TFunction<void(ACommuter*)>& Callback)
{
	// Only loop based on how many we need to activate
	// Once there are no more we can stop
	for (int32 Index = 0; Pool.IsValidIndex(Index) && NumberToActivate > 0; ++Index)
	{
		FCommuterNode& Node = Pool[Index];
		if (!Node.bIsActive)
		{
			Node.bIsActive = true;

			Callback(Node.Commuter);

			--NumberToActivate;
		}
	}
}

bool FCommuterPool::Deactivate(ACommuter* Commuter)
{
	if (!Commuter)
	{
		return false;
	}

	FCommuterNode* Node = Pool.FindByPredicate(
		[Commuter](const FCommuterNode& Node)->bool
	{
		return Node.Commuter == Commuter;
	});

	// If node is not valid, we skip checking
	// if commuter was just deactivated
	bool bDeactivated = Node != nullptr;

	if (bDeactivated)
	{
		// We're first assigning deactived to be isActive.
		// If the commuter was active, it means we will be deactivating it.
		bDeactivated = Node->bIsActive;

		if (bDeactivated)
		{
			Node->bIsActive = false;
		}
	}

	return bDeactivated;
}

ACommuter* FCommuterPool::Deactivate(int32 Index)
{
	if (Pool.IsValidIndex(Index))
	{
		FCommuterNode& Node = Pool[Index];

		if (Node.Commuter && Node.bIsActive)
		{
			Node.bIsActive = false;
			return Node.Commuter;
		}
	}

	return nullptr;
}

ARoadBlazeLevelScriptActor::ARoadBlazeLevelScriptActor()
{
	bAlignLanesWithPlayer = true;
	Commuters.CommuterTemplate = ACommuter::StaticClass();
}

#if WITH_EDITOR

void ARoadBlazeLevelScriptActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (!Commuters.CommuterTemplate)
	{
		Commuters.CommuterTemplate = ACommuter::StaticClass();
	}
}

#endif

void ARoadBlazeLevelScriptActor::InitCommuterPool(int32 Size)
{
	TFunction<void(ACommuter*)> Callback = 
		[this](ACommuter* Commuter)->void
	{
		HideCommuter(Commuter);
	};

	Commuters.Resize(Size, GetWorld(), Callback);
}

void ARoadBlazeLevelScriptActor::InitPlayer()
{
	if (PlayersDodger)
	{
		USkeletalMeshComponent* Dodger = PlayersDodger->GetMesh();
		Dodger->OnComponentHit.AddDynamic(this, &ARoadBlazeLevelScriptActor::OnDodgerHit);
	}
}

void ARoadBlazeLevelScriptActor::SpawnCommuters(int32 Amount)
{
	if (LevelRoad)
	{
		TFunction<void(ACommuter*)> SetCallback =
			[this](ACommuter* Commuter)->void
		{
			RespawnCommuter(Commuter);
			OnCommuterSpawn.Broadcast(Commuter);
		};

		Commuters.Activate(Amount, SetCallback);
	}
}

void ARoadBlazeLevelScriptActor::InitRoad()
{
	// If no road was set, try to find one we can use
	if (!LevelRoad)
	{
		TArray<AActor*> Roads;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ARoad::StaticClass(), Roads);

		// If a road was found, lets use it instead
		if (Roads.Num() > 0)
		{
			LevelRoad = Cast<ARoad>(Roads[0]);
		}
	}

	// Double check in-case no road exists in the level
	if (LevelRoad)
	{
		LevelRoad->OnCommuterReachedEnd.AddDynamic(this, &ARoadBlazeLevelScriptActor::OnCommuterAtEnd);

		// Set the player to view the road from the provided viewing camera
		if (PlayersDodger)
		{
			APlayerController* Player = Cast<APlayerController>(PlayersDodger->GetController());
			
			// Make sure a player is actually controlling the dodger
			if (Player)
			{
				Player->SetViewTarget(LevelRoad);
			}

			if (bAlignLanesWithPlayer)
			{
				// Since lanes are in local space of the road. The Z translation needs
				// to be the displacement between the root of the road and the player
				float ZLocation = PlayersDodger->GetActorLocation().Z - LevelRoad->GetActorLocation().Z;

				TArray<FLane>& Lanes = LevelRoad->GetLanes();

				for (FLane& Lane : Lanes)
				{
					FVector NewLocation = Lane.Transform.GetTranslation();
					NewLocation.Z = ZLocation;
					Lane.Transform.SetTranslation(NewLocation);
				}
			}
		}
	}
}

void ARoadBlazeLevelScriptActor::HideCommuter(ACommuter* Commuter)
{
	if (OffscreenCommuterPlacementActor && Commuter)
	{
		FTransform Offscreen = OffscreenCommuterPlacementActor->GetActorTransform();
		Commuter->SetActorTransform(Offscreen);
	}
}

void ARoadBlazeLevelScriptActor::OnCommuterAtEnd(ACommuter* Commuter, const FLane& Lane, int32 LaneIndex)
{
	// First de-activate the commuter
	Commuters.Deactivate(Commuter);
	
	Commuter->GetLaneMovementComponent()->Deactivate();
	HideCommuter(Commuter);
	OnCommuterDespawn.Broadcast(Commuter);
}

void ARoadBlazeLevelScriptActor::OnDodgerHit(UPrimitiveComponent* HitComponent, 
	AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// We are only interested if the player has collided with a commuter
	ACommuter* Commuter = Cast<ACommuter>(OtherActor);
	if (Commuter)
	{
		OnDodgerDeath.Broadcast(PlayersDodger, Commuter);
	}
}

void ARoadBlazeLevelScriptActor::RespawnCommuter(ACommuter* Commuter)
{
	if (LevelRoad)
	{
		int32 LaneIndex = LevelRoad->GetRandomLaneIndex();
		LevelRoad->SetActorAtLane(Commuter, LaneIndex);

		Commuter->LaneIndex = LaneIndex;

		USingleLaneMovementComponent* LaneMovement = Commuter->GetLaneMovementComponent();
		LaneMovement->Activate();
	}
}
