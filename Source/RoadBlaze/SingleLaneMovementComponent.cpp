// Fill out your copyright notice in the Description page of Project Settings.

#include "SingleLaneMovementComponent.h"

USingleLaneMovementComponent::USingleLaneMovementComponent()
{
	Speed = 300.f;
}

void USingleLaneMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if (ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (UpdatedComponent)
	{
		FVector Forward = UpdatedComponent->GetForwardVector();
		FVector DesiredMovement = Forward * Speed;

		DesiredMovement *= DeltaTime;

		// Don't bother trying to move if we wouldn't be moving
		if (!DesiredMovement.IsNearlyZero())
		{
			FHitResult Hit;
			SafeMoveUpdatedComponent(DesiredMovement, UpdatedComponent->GetComponentRotation(), true, Hit);

			// Try sliding along any hit surface
			if (Hit.IsValidBlockingHit())
			{
				SlideAlongSurface(DesiredMovement, 1.f - Hit.Time, Hit.Normal, Hit, true);
			}
		}
	}
}
